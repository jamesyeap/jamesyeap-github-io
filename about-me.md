---
layout: default
slug: about me
---

I'm typing this out as I procrastinate on doing the work that should really matter more to me (studying for mid-terms), so that's probably all you need to know about me (and my priorities in life - or the lack thereof).

However, since I've already started on this, and until the work sitting in front of me begins to somehow look more appealing, I guess I will continue. 

As of 2020, I am 22 years old. 

I am currently a freshman at the ~~in~~famous National University of Singapore. 

Like many other people who don't know what to do with themselves, I chose to major in Computer Science. Also, sitting in front of a computer in an air-conditioned room holds more appeal for me than working in a chemical plant (I studied Chemical Engineering in polytechnic.)

~~If you're reading this and happen to be from the human resources department from a company that I've applied to, you should totally ignore the paragraph above and stick with the story that I gave during our interview instead.~~ 

So yeah, I'll admit that I don't really have a passion for anything; hopefully the next four years will let me get closer to finding one. 

Okay I think I should get back to doing work lol. 



