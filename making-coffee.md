---
layout: default
slug: making coffee
---

# Making Coffee

I think I've realised why I only drink coffee with ice: I've been drinking bad coffee all along. 

Since my first encounter with this magical beverage in secondary school, I've always resorted to downing this stuff ice-cold, because anything else just tasted plain horrible. 

A couple of years later, through a mixture of boredom, some hippie-coffee videos and some money dumped into marketing hippie-coffee to normie Singaporeans like myself, I managed to convince myself that buying a Chemex, a hand-grinder and some hippie-coffee-beans would be a good idea, and so I did. 

And I don't regret it, because hippie-coffee tastes quite a bit better than the normie, powdered stuff. 

For one, you can actually drink it warm and not feel like you're drinking some herbal concoction your grandma probably made you drink every time she thought you looked a little sickly, which is what powdered coffee begins to taste like a few days after you tear the seal off and smell (for the first time since you last opened another bottle of powdered coffee) what coffee should smell like. 

I've only gone through two bags of beans, so my analysis of why this is so can only be as nuanced as this: idk there's just more stuff besides the usual bitterness slapping you in the face when you drink it? 

Not much help I know. 

I think I'll have more to say when I'm further along in my journey towards hippie-coffee-snobhood. 

So, if for some reason you're 
- on my site (hello HR please give me the internship I really need the ~~money and hopefully subsequent job offer~~ learning experience!) 
- have a love-hate relationship with coffee (hello burnt-out JC kids)
- have some spare cash lying around (freshly-ORD-ed boys who didn't give all your money to the canteen uncles and aunties / smoked it all away)

perhaps give this thing a try? 

The hippies may actually be onto something legit this time. 

/// 

## Stuff to get yourself if you want to try this shit out

- 1x kettle (preferably one with a gooseneck)
- 1x Chemex OR Hario V60 (or equivalent)
- Filter papers for your Chemex/Hario V60 
- Hippie coffee beans 
- 1x hand-grinder (I got a Porlex Mini, although I really wish that I've gotten the tall-variant because then I can make more grounds at one go to fully utilise my 6-cup Chemex)
- 1x precision scale (don't cheap out on this shit it really makes your life easier)

/// 

## So far, I've tried the following bags: 

| Name | Thoughts                    |
|  :--- | :---------------------------- |
| Ethiopia Warqee Benti Nenka | These tasted really good, and they smelled sweet for some reason. |
| Honduras Pantanal | These tasted bitter.  |

*P.S. HR please gimme job/internship*










